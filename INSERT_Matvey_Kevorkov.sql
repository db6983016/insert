-- 1: Insert the film
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update)
VALUES ('Poor Things', 'A woman is brought back to life by an unorthodox scientist.', 2023, (SELECT language_id FROM language WHERE name = 'English'), 14, 4.99, 141, 19.99, 'R', NOW());

-- 2: Insert actors into the "actor" table
INSERT INTO actor (first_name, last_name, last_update)
VALUES ('Emma', 'Stone', NOW()),
       ('Mark', 'Ruffalo', NOW()),
       ('Willem', 'Dafoe', NOW());

-- 3: Link actors to the film in the "film_actor" table
INSERT INTO film_actor (actor_id, film_id, last_update)
VALUES ((SELECT actor_id FROM actor WHERE first_name = 'Emma' AND last_name = 'Stone'), (SELECT film_id FROM film WHERE title = 'Poor Things'), NOW()),
       ((SELECT actor_id FROM actor WHERE first_name = 'Mark' AND last_name = 'Ruffalo'), (SELECT film_id FROM film WHERE title = 'Poor Things'), NOW()),
       ((SELECT actor_id FROM actor WHERE first_name = 'Willem' AND last_name = 'Dafoe'), (SELECT film_id FROM film WHERE title = 'Poor Things'), NOW());

-- 4: Insert 2 more films
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update)
VALUES ('Inception', 'A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a CEO.', 2010, (SELECT language_id FROM language WHERE name = 'English'), 14, 4.99, 148, 19.99, 'PG-13', NOW());

INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update)
VALUES ('The Matrix', 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.', 1999, (SELECT language_id FROM language WHERE name = 'English'), 14, 4.99, 136, 19.99, 'R', NOW());

-- 5: Insert all films into inventory for store 1
INSERT INTO inventory (film_id, store_id, last_update)
VALUES ((SELECT film_id FROM film WHERE title = 'Poor Things'), 1, NOW());

INSERT INTO inventory (film_id, store_id, last_update)
VALUES ((SELECT film_id FROM film WHERE title = 'Inception'), 1, NOW());

INSERT INTO inventory (film_id, store_id, last_update)
VALUES ((SELECT film_id FROM film WHERE title = 'The Matrix'), 1, NOW());
